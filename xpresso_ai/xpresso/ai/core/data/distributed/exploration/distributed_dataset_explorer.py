"""Module for data understand and exploration"""

__all__ = ['Explorer']
__author__ = 'Sanyog Vyawahare'

import pandas as pd

from xpresso.ai.core.commons.exceptions.xpr_exceptions import InputLengthMismatch
from xpresso.ai.core.commons.utils.constants import \
    EXPLORE_UNIVARIATE_FILENAME, \
    EXPLORER_OUTPUT_PATH, DEFAULT_PROBABILITY_BINS, EXPLORE_MULTIVARIATE_FILENAME
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.data.exploration.render_exploration import RenderExploration
from xpresso.ai.core.data.visualization.abstract_visualization import PlotType
from xpresso.ai.core.logging.xpr_log import XprLogger

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class Explorer:
    """Understands and Explore the data"""

    def __init__(self, dataset):
        self.dataset = dataset

    def understand(self, verbose=True):
        """Understands and assigns the datatype to the attributes
        Args:
            verbose('bool'): If True then renders the output"""
        if not isinstance(self.dataset.type, DatasetType):
            logger.warning(
                "Unsupported datatype provided to understand")
            return

        self.dataset.info.understand_attributes(self.dataset.data, self.dataset.type)
        for attr in self.dataset.info.attributeInfo:
            if attr.type == DataType.NUMERIC.value:
                self.dataset.data[attr.name] = self.dataset.data[attr.name].astype(
                    DataType.FLOAT.value)

        RenderExploration(self.dataset).render_understand(verbose=verbose)

    def explore_univariate(self, verbose=True, to_excel=False,
                           validity_threshold=None,
                           output_path=EXPLORER_OUTPUT_PATH,
                           file_name=EXPLORE_UNIVARIATE_FILENAME,
                           bins=DEFAULT_PROBABILITY_BINS):
        """
        Univariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
            validity_threshold(int): percent value for garbage threshold
            bins(int): No. of buckets for bar graph, default 20
        """

        if self.dataset.type is not DatasetType.DIST_STRUCTURED or not isinstance(
                self.dataset.type, DatasetType):
            logger.warning(
                "Unsupported datatype provided to explore_univariate")
            return
        self.dataset.info.populate_attribute(self.dataset.data,
                                             self.dataset.type,
                                             validity_threshold,
                                             bins=bins)

        RenderExploration(self.dataset).render_univariate(
            verbose=verbose, to_excel=to_excel, output_path=output_path,
            file_name=file_name)

    def explore_multivariate(self, verbose=True, to_excel=False,
                             output_path=EXPLORER_OUTPUT_PATH,
                             file_name=EXPLORE_MULTIVARIATE_FILENAME):
        """
        Multivariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
        """

        # if self.dataset.type != DatasetType.STRUCTURED or not isinstance(
        #         self.dataset.type, DatasetType):
        #     logger.warning(
        #         "Unsupported datatype provided to explore_multivariate")
        #     return

        # self.dataset.info.populate_metric(self.dataset.data, self.dataset.type)

        # RenderExploration(self.dataset).render_multivariate(
        #     verbose=verbose, to_excel=to_excel, output_path=output_path,
        #     file_name=file_name)
